/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   punteros.h
 * Author: oliva
 *
 * Created on 7 de julio de 2016, 05:15 PM
 */

#ifndef PUNTEROS_H
#define PUNTEROS_H

#include <stdlib.h>
#include <iostream>
#include <time.h>

using namespace std;

class punteros {
public:
    punteros();
    punteros(const punteros& orig);
    virtual ~punteros();
private:
    void mostarVariable(string nombre,int * ptr);
    void inicializarNumeroAleatorioPtr(int ** ptr);
    void inicializarNumeroAleatorio(int *ptr);
    int generarNumeroAleatorio();

};

#endif /* PUNTEROS_H */

