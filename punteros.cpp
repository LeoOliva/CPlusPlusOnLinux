/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   punteros.cpp
 * Author: oliva
 * 
 * Created on 7 de julio de 2016, 05:15 PM
 */
/*
 * DEFINICIÓN:
 * Un puntero es un dato que contiene una dirección de memoria
 * 
 * NOTA: Existe una dirección especial que representa por medio  de la constante
 * NULL (definida en <stdlib.h>) y se emplea cuando queremos indicar que un 
 * puntero no apunta a ninguna dirección.
 * 
 * -----------------------------------------------------------------------------
 * 
 * DECLARACIÓN:
 *  <tipo>   *<identificador>
 *      
 *            <tipo> Tipo de dato del objeto rederenciado por el puntero.
 *            <identificador> Identficador de la variable de tipo puntero.
 * Cuando se declara un puntero se reserva memoria para albergar una dirección de
 * memoria, pero NO PARA ALMAENAR EL DATO AL QUE APUNTA EL PUNTERO.
 * 
 * El espacio de memoria reserado para almacenar un puntero es el mismo
 * idependientemente del tipo de dato al que apunte: El espacio que ocupa una
 * dirección de memoria.
 * 
 * -----------------------------------------------------------------------------
 * 
 * OPRECAIONES BÁSICAS CON PUNTEROS.
 * 
 * DIRECCIÓN  ( OPERADOR & )
 * &<id> devuelve la dirección de memoria donde comienza la variable <id>.
 * El operador & se utiliza para asignar valores a datos de tipo puntero:
 *      int i;
 *      int *ptr;
 *      ptr = &i;
 * 
 * INDIRECCIÓN  ( OPERADOR * )
 * *<ptr> devuelve el contenido del objeto referenciado por el puntero <ptr>
 * El operador * se usa para acceder a los objetos a los que apunta un puntero:
 *      char c;
 *      char *ptr;
 *      ptr = &c;
 *     *ptr = 'A'; // Equivale a escribir: c = 'A'
 * 
 * ASIGNACIÓN ( OPERADOR = )
 * A un puntero se le puede asignar una dirección de memoria concreta, la 
 * dirección de una variable o el contenido de otro puntero. 
 * 
 * Una direccion de memoria concreta:
 *      int *ptr;
 *      ptr = 0x1F3CE00A;
 *      ptr = NULL;
 * La dirección de una variable del tipo al que apunto el puntero:
 *      char c;
 *      char *ptr;
 *      ptr = &c;
 * Otro puntero del mismo tipo:
 *      char c;
 *      char *ptr1;
 *      char *ptr2;
 *      ptr1 = &c;
 *      ptr2 = ptr1;
 * 
 * NOTA:
 * Como todas la variables, los punteros tambien contiene "basura" cuando se
 * declaran, por lo que es una buena costumbre inicializarlos con NULL
 * 
 * 
 *      
 *  
 * 
 * 
 */

#include "punteros.h"


punteros::punteros() {
    //Inicializamos el random
    srand(time(NULL));
    
    int *p = 0;
    inicializarNumeroAleatorioPtr(&p);
    mostarVariable("p",p);
    
    int *q = NULL;
    inicializarNumeroAleatorio(q);
    mostarVariable("q",q);
    
    

    
    
    
}

punteros::punteros(const punteros& orig) {
}

punteros::~punteros() {
}

void punteros::mostarVariable(string nombre,int * ptr){
        cout<<"Valor de variable "<<nombre<<": "<<*ptr<<endl;
    cout<<"Dirección de varaible "<<nombre<<": "<<ptr<<endl;
}

int punteros::generarNumeroAleatorio(){
    //Numeros aleratorio de 1 al 10
    int aux = rand() % (11 - 1) + 1;
    cout<<"Número aleatorio generado: " << aux<<endl;
    return aux;
}

//Recive la dirección de un entero
void punteros::inicializarNumeroAleatorio(int * ptr){
    //Manera INCORRECTA de inicializar un puntero por medio
    //de una función, solo le cambia el valor de manera local
    
     ptr = reinterpret_cast<int *> (malloc(sizeof(int)));
    *ptr = generarNumeroAleatorio();
    mostarVariable("ptr",ptr);
}

//Recive la direccion de un puntero
void punteros::inicializarNumeroAleatorioPtr(int **ptr){
    //La manera correcta de inicializar un puntero por medio
    //de una funcion
    
    //Reservamos la memoria
    *ptr = reinterpret_cast<int *> (malloc(sizeof(int)));
    //inicializamos
    **ptr = generarNumeroAleatorio();
}


